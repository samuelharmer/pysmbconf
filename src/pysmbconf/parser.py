import os
import re
import logging
from pathlib import Path, PurePath, PurePosixPath
from typing import TextIO

UNKNOWN = object()


def fullname(o):
    """
    o.__module__ + "." + o.__class__.__qualname__ is an example in this
    context of H.L. Mencken's "neat, plausible, and wrong." Python makes no
    guarantees as to whether the __module__ special attribute is defined, so
    we take a more circumspect approach. Alas, the module name is explicitly
    excluded from __qualname__ in Python 3.

    Source: https://stackoverflow.com/a/2020083/594137
    """

    module = o.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return o.__class__.__name__  # Avoid reporting __builtin__
    else:
        return module + '.' + o.__class__.__name__


class MockHost:
    def __init__(self, **kwargs):
        netloc = kwargs.get('netloc', UNKNOWN)

        self._name_parts = [] if netloc is UNKNOWN else \
            list(filter(None, netloc.split('.')))

    @property
    def hostname(self):
        return self._name_parts[0] if self._name_parts else UNKNOWN

    @property
    def dnsdomain(self):
        domain_parts = self._name_parts[1:]
        return '.'.join(domain_parts) if domain_parts else UNKNOWN

    @property
    def netloc(self):
        return self.hostname if self.dnsdomain is UNKNOWN else \
            '.'.join([self.hostname, self.dnsdomain])

    @property
    def _canonical_netloc(self):
        return UNKNOWN if self.netloc is UNKNOWN else \
            '.'.join(self.netloc).casefold()

    def __hash__(self):
        return hash(UNKNOWN) if self._canonical_netloc is UNKNOWN else \
            hash(self._canonical_netloc)

    def __eq__(self, other):

        if not isinstance(other, MockHost):
            return False

        if self.netloc == UNKNOWN and other.netloc == UNKNOWN:
            return True

        return self._canonical_netloc == other._canonical_netloc

    def __repr__(self):
        constructor = fullname(self)
        if self.netloc is UNKNOWN:
            constructor += '()'
        else:
            constructor += '(netloc={})'.format(repr(self.netloc))
        return constructor

    def __str__(self):
        return 'UNKNOWN' if self.netloc is UNKNOWN else self.netloc


class MockServer(MockHost):
    pass


class MockClient(MockHost):
    pass


UNKNOWN_SERVER = MockServer()
UNKNOWN_CLIENT = MockClient()


class SambaConfigParser:
    """Create an object which represents a parsed Samba configuration file."""

    def __init__(self, fake_root: Path, smb_conf: PurePosixPath):

        # Store and check the root directory exists
        self.fake_root = fake_root.absolute()
        os.stat(str(self.fake_root))

        # Store initial smb.conf
        self.smb_conf = smb_conf

        # This path should appear to be absolute, but is treated as relative
        # to the fake_root. This allows us to parse smb.conf files without
        # needing to run this using chroot, or on the actual Samba host.
        if not smb_conf.is_absolute():
            raise ValueError('smb_conf must be absolute ("{smb_conf}")'.format(
                smb_conf=smb_conf))

        # Check it exists
        os.stat(str(self.localize(smb_conf)))

        # Set the context to UNKNOWN until we contextualize
        self._contexts = {
            MockServer: UNKNOWN_SERVER,
            MockClient: UNKNOWN_CLIENT
        }

    def parse(self):
        with self.localize(self.smb_conf).open() as f:
            return [token for token in UnknownToken.tokenize(f)]

    def contextualized(self, *contexts):
        """Returns a new SambaConfigParser with the context
        of an SMB connection."""

        unrecognized = []
        extraneous = []
        new_contexts = {}

        for new_context in contexts:

            current_context = self._contexts.get(type(new_context))

            # Avoid objects of types which aren't defined in the constructor
            if current_context is None:
                unrecognized.append(new_context)
                continue

            # There should only be one object of any particular type
            if type(new_context) in new_contexts:
                extraneous.append(new_context)
                continue

            msg = 'Using {}'.format(new_context)
            if new_context != current_context:
                msg += ' in place of {}'.format(current_context)
            logging.info(msg)

            new_contexts[type(new_context)] = new_context

        if unrecognized:
            logging.warning('Unrecognized contexts: {objects}'.format(
                objects=', '.join([repr(o) for o in unrecognized])))

        if extraneous:
            logging.warning('Extraneous contexts: {objects}'.format(
                objects=', '.join([repr(o) for o in extraneous])))

        conf = SambaConfigParser(fake_root=self.fake_root,
                                 smb_conf=self.smb_conf)
        conf._contexts.update(new_contexts)
        return conf

    def localize(self, path: PurePath):
        if path.is_absolute():
            path_type = type(path)
            return self.fake_root / path_type(*path.parts[1:])
        else:
            raise ValueError(
                'Relative path passed to `localise`: {path}'.format(path=path))


class UnknownToken:
    """A token in this context is one declaration of either blank, comment,
    section name, parameter, or unknown. The token produced contains the
    original value without modification. I.e. the original file can be
    reconstructed from the tokens produced."""

    @staticmethod
    def tokenize(f: TextIO):
        position = f.tell()

        # Grab the first line
        next_line = f.readline()

        # Whilst we have a line (even an empty one will contain a newline)
        while next_line:

            # Processes any line continuations
            while len(next_line) > 1 and next_line[-2:] == '\\\n':
                next_line += f.readline()

            # Turn the line into a token, and set its position
            token = UnknownToken(next_line).specialized()
            token.position = position

            yield token

            # Advance the iterator
            position += len(token)
            next_line = f.readline()

    @classmethod
    def token_classes(cls):
        """Recursively gather my subclasses and their subclasses into a set."""
        return set(cls.__subclasses__()).union(
            [s for c in cls.__subclasses__() for s in c.token_classes()])

    _cre = None
    _cre_line_wrap = re.compile(r'\\\n')
    _cre_whitespace = re.compile(r'\s+')

    def __init__(self, raw_value):
        self.raw_value = raw_value
        self.position = 0

        # First we check the token doesn't contain un-escaped newlines.
        # Escaped newlines are fine as is the line-terminating newline.
        unwrapped = self._cre_line_wrap.sub('', raw_value)
        if '\n' in unwrapped[:-1]:
            raise ValueError('Un-escaped newlines are illegal within tokens: '
                             '{}'.format(repr(unwrapped)))

        self._validate_raw_value(raw_value)

    def _validate_raw_value(self, raw_value):
        """Effectively a no-op for the UnknownToken class, but for subclasses
        of UnknownToken which override _cre with a compiled regular expression
        for validation, this will ensure we don't create instances of them with
        an invalid raw_value."""
        if self._cre is not None and not self._cre.match(self.cleansed):
            msg = '"{}" is not a ' + type(self).__name__
            raise ValueError(msg.format(raw_value))

    def _normalize_whitespace(self, value):
        """Remove leading and trailing space, then replace contiguous
        whitespace with a single space."""
        return self._cre_whitespace.sub(' ', value.strip())

    @property
    def cleansed(self):
        """Remove internal line wraps, leading and trailing whitespace."""
        return self._cre_line_wrap.sub('', self.raw_value).strip()

    def specialized(self):
        """Return a new instance of the most specialized class which can be
        created with this token's raw_value. If the raw_value can be
        interpreted in more than one way at any given level of specificity,
        the next level of specificity up will be returned."""

        answers = []
        subclasses = self.token_classes()

        # Try to create an instance of all subclasses and store those which
        # succeed as possible answers.
        for sc in subclasses:
            try:
                answers.append(
                    (sc(self.raw_value), len(sc.mro()))
                )
            except ValueError:
                continue

        # No matches so it must be unknown
        if len(answers) == 0:
            return self

        # One match so that must be it
        if len(answers) == 1:
            return answers.pop()[0]

        # We got more than one match. This might still be OK as more specific
        # tokens can be subclasses of less specific tokens. I.e.
        # All IncludeParameterTokens are valid ParameterTokens, but not all
        # ParameterTokens are valid IncludeParameterTokens.

        # We arbitrarily conclude that a class's inheritance depth equates to
        # its specificity. This may not be a good idea, but it works for now.
        # To establish a class's inheritance depth, we use the length of its
        # Method Resolution Order (MRO) obtained above.
        #
        # https://www.python.org/download/releases/2.3/mro/

        # Sort answers on their MRO, followed by their name
        answers = sorted(answers,
                         key=lambda a: (a[1], a[0].__class__.__name__))

        # To confirm we have one specific answer we must compare the MRO of the
        # last answer with that of the penultimate answer.
        last, last_depth = answers.pop()
        penultimate, penultimate_depth = answers.pop()
        if last_depth > penultimate_depth:
            return last

        # If we arrive here, it's because we have managed to parse the token as
        # two different types which are equally 'specific'.
        # This could mean we haven't designed our classes' regular expressions
        # in sufficient detail.
        raise ValueError(
            'Multiple matches for {raw_value}: {answers}'.format(
                raw_value=repr(self.raw_value), answers=answers))

    def __repr__(self):
        return '{}(position={}, value={})'.format(
            fullname(self), repr(self.position), repr(self.raw_value))

    def __str__(self):
        return self.raw_value

    def __len__(self):
        return len(self.raw_value)


class BlankToken(UnknownToken):
    """Lines composed exclusively of whitespace are ignored."""

    _cre = re.compile(r'^\s*$')


class CommentToken(UnknownToken):
    """Comments begin with either a hash (`#`)or a semicolon (`;`).
    Leading and trailing whitespace is ignored. The entire line is ignored."""

    _cre = re.compile(r'^[;#](?P<value>.*)$')

    @property
    def comment(self):
        return


class SectionNameToken(UnknownToken):
    """Section names are enclosed with square brackets. Leading and trailing
    whitespace is ignored, internal whitespace is irrelevant. Section names
    are not case-sensitive."""

    _cre = re.compile(r'^\[(?P<section_name>[^=]+)]$')

    @property
    def section_name(self):
        section_name = self._cre.match(self.cleansed)['section_name']
        return self._normalize_whitespace(section_name)


class ParameterToken(UnknownToken):
    """
    Parameters take the form `key=value`.

    For the key,
    - leading and trailing whitespace is ignored,
    - internal whitespace is irrelevant,
    - it is case-insensitive,
    - it is case-preserving.

    For the value,
    - leading and trailing whitespace is ignored,
    - internal whitespace is preserved,
    - case is preserved.
    """

    _cre = re.compile(r'^(?P<key>[^;#[][^=]*)\s*=\s*(?P<value>.*)$')

    @property
    def key(self):
        key = self._cre.match(self.cleansed)['key']
        return self._normalize_whitespace(key)

    @property
    def value(self):
        return self._cre.match(self.cleansed)['value']


class IncludeParameterToken(ParameterToken):

    _cre = re.compile(r'^(?P<key>include)\s*=\s*(?P<value>.*)$')


class OldSambaConfigParserFunctions:
    def alt_conf(self):
        # Check for a global configuration file definition which would extend
        # the configuration file for a specific client's connection.
        # https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm1926
        if self.has_option('global', 'config file'):

            # Define alternative configuration dictionary
            self.alternative_confs = {}

            # Expand the configuration file path
            self.alternative_conf_re = \
                self.expand_samba_path(self.get('global', 'config file'))

            # Recursively loop through directories and files in
            # the server's folder looking for paths which match
            # the alternative configuration pattern
            for root, subdirs, files in os.walk(self.root_path):
                for file in files:
                    matches = self.alternative_conf_re.search(
                        os.path.join(root, file))
                    if matches:
                        # Unfortunately some people don't check what they're
                        # writing in a configuration file makes sense. The
                        # following test prevents a self inclusion causing an
                        # infinite loop.
                        if matches.group() == self.base_conf_path:
                            raise SyntaxError(
                                "Samba configuration file \"%s\" on "
                                "%s attempts to include \"%s\" (which would "
                                "be a recursive include)." % (
                                    self.base_conf_path,
                                    self.root_path,
                                    self.get("global", "config file", True)))

                        # Parse the alternative configuration file and store it
                        # under the name of the file.
                        self.alternative_confs[matches.group()] = \
                            SambaConfigParser(self.root_path, matches.group())

    def get_paths(self):
        # Create container for the paths
        unique_paths = set()

        # Extract all of this configuration's paths
        for section in self.sections():
            if self.has_option(section, "path"):
                unique_paths.add(
                    os.path.normpath(self.get(section, "path", True)))

        # Extract any alternative configuration's paths
        if hasattr(self, 'alternative_confs'):
            for alt_conf_path, alt_conf in self.alternative_confs.items():
                unique_paths.union(alt_conf.get_paths())

        # Return a sorted list of all the paths discovered
        return sorted(list(unique_paths))

    @staticmethod
    def expand_samba_path(p):
        """
        Converts a Samba path to a RegexObject.

        Takes a Samba path (which may include variables) and returns the
        equivalent compiled regular expression. Because we currently only use
        paths with the Client's NetBIOS name ("%m") this is the only thing this
        pattern expands at present.

        For full details of Samba path variables see
        http://www.samba.org/samba/docs/using_samba/ch06.html#samba2-CHP-6-TABLE-2
        """
        # Convert the file's name to an exact regular expression pattern
        p = re.escape(p)

        # Convert any Samba variables (which are now escaped)
        # into named regular expression patterns.
        substitutions = [
            # At present we only use %m
            (re.escape('%m'), '(?P<netbios_name>.*)')
        ]
        for search, replacement in substitutions:
            p = p.replace(search, replacement)

        return re.compile(p)
