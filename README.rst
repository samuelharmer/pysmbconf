
Python smb.conf Parser
======================

Attempting to use ConfigParser on smb.conf just won't work. The file format is
not standard INI format for at least two reasons.

1. The `include (S)`_ parameter may be used more than once. Normally duplicate
   parameters would either be illegal or the last would *win*. In Samba each
   ``include`` is processed as it is found [1]_.
2. If the `config file (G)`_ parameter points to a file other than the one it
   is in, parsing restarts with the new configuration file. The file containing
   the ``config file`` parameter is not revisited [2]_.
3. `Variable Substitutions`_ permit use of variables within parameter values to
   dynamically alter the configuration based on properties of the SMB
   connection (e.g. host, client, or time).
4. Other parameters can also have a significant effect on the resulting
   configuration. For example, `copy (S)`_, `change share command (G)`_, and
   `config backend (G)`_.

This library is designed to audit Samba configuration taking these unique
features into account.

.. _Variable Substitutions: https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm191
.. _change share command (G): https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm1412
.. _include (S): https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm4014
.. _config backend (G): https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm1902
.. _config file (G): https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm1926
.. _copy (S): https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#idm1945

.. [1] It is not clear from the documentation whether parameters following the
       ``include`` are interpreted as if they were in the same section as the
       ``include``, or whether they belong to the final section of the included
       file. The former seems the logical choice.

.. [2] It is not clear whether already-parsed parameters are remembered or
       forgotten when changing file.
