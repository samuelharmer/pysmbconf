"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
import os
import re
import itertools
from collections import defaultdict

here = os.path.abspath(os.path.dirname(__file__))
src_dir = 'src'


def find_var(filename, varname):
    pattern = r"^{name} = '(?P<value>[^']*)'$".format(name=varname)
    prog = re.compile(pattern, flags=re.MULTILINE | re.IGNORECASE)
    for root, dirs, files in os.walk(src_dir):
        for file in files:
            if file == filename:
                with open(os.path.join(root, file)) as f:
                    contents = f.read()
                    result = prog.search(contents)
                    if result:
                        value = result.group('value')
                        if value:
                            return value
    return None


program_version = find_var('__init__.py', '__version__')
program_name = find_var('__init__.py', 'program_name')
rel_package_dir = os.path.join(src_dir, program_name)


# Requirements
# ============
# If these change run `tox --recreate`
# https://tox.readthedocs.io/en/latest/example/basic.html#forcing-re-creation-of-virtual-environments
reqs = defaultdict(list)

# Basic
# -----
reqs['parser'] += []

# Testing
# -------
test_reqs = ['check-manifest', 'coverage', 'docutils', 'flake8', 'pytest',
             'tox']

# Development
# -----------
dev_reqs = ['setuptools', 'wheel']

flat_reqs = sorted(set(itertools.chain.from_iterable(reqs.values())))


# Get the long description from the README file
with open(os.path.join(here, 'README.rst'), encoding='utf-8') as readme_file:
    long_description = readme_file.read()

setup(
    name=program_name,
    version=program_version,

    description='Python smb.conf Parser',
    long_description=long_description,

    url='https://gitlab.com/samuelharmer/pysmbconf',

    author='Samuel Harmer',
    author_email='incoming+samuelharmer-pysmbconf-12839725-issue-'
                 '@incoming.gitlab.com',

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=flat_reqs,

    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
        'test': test_reqs,
        'dev': dev_reqs + test_reqs,
    },

    packages=find_packages(src_dir),
    package_dir={'': src_dir},

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    # entry_points={
    #     'console_scripts': [
    #         'pytestparm=pysmbconf.cli:testparm',
    #     ],
    # },
)
