
import pytest
from pathlib import PurePosixPath, PureWindowsPath

from .common import test_data

from pysmbconf import (
    SambaConfigParser,
    UnknownToken, BlankToken, CommentToken, SectionNameToken, ParameterToken,
    MockServer, MockHost, UNKNOWN)


class TestMockHost:

    def test_unknown_host(self):
        """If we don't provide any configuration, everything is UNKNOWN."""
        assert MockHost().netloc == UNKNOWN
        assert MockHost().hostname == UNKNOWN
        assert MockHost().dnsdomain == UNKNOWN

    def test_dns_netloc(self):
        """If we provide a DNS name, everything should be populated."""
        assert MockHost(netloc='HOST.example.com').netloc == 'HOST.example.com'
        assert MockHost(netloc='HOST.example.com').hostname == 'HOST'
        assert MockHost(netloc='HOST.example.com').dnsdomain == 'example.com'

    def test_netbios_netloc(self):
        """If we only provide a NetBIOS name, the dnsdomain must be UNKNOWN."""
        assert MockHost(netloc='HOST').hostname == 'HOST'
        assert MockHost(netloc='HOST').netloc == 'HOST'
        assert MockHost(netloc='HOST').dnsdomain == UNKNOWN

    def test_equality(self):
        """Check equality operator."""
        assert MockHost() == MockHost()
        assert MockHost(netloc='') == MockHost(netloc='')
        assert MockHost(netloc='HOST') == MockHost(netloc='host')
        assert \
            MockHost(netloc='HOST.EXAMPLE.COM') == \
            MockHost(netloc='host.example.com')


class TestUnknownToken:
    """The UnknownToken class is the base of all tokens so contains a large
    amount of generic-token functionality."""

    unknown_tokens = [
        'x',
        '[Incomplete a section name',
        'Incomplete a section name]',
        ']Backwards section name[',
        'Not a token!',
        '=',
        '[weird = section = name]',
    ]

    @pytest.mark.parametrize('raw_value', unknown_tokens)
    def test_initialization(self, raw_value):
        """The UnknownToken class is the catch-all for any garbage."""
        assert UnknownToken(raw_value) is not None

    @pytest.mark.parametrize('raw_value', unknown_tokens)
    def test_specialization(self, raw_value):
        """Specializing an unknown token is a no-op."""
        original = UnknownToken(raw_value)
        assert original is original.specialized()

    def test_newlines(self):
        """Escaped newlines are fine. Unescaped newlines are invalid."""
        assert UnknownToken("one \\\n line") is not None

        with pytest.raises(
                ValueError,
                match='Un-escaped newlines are illegal within tokens'):
            assert UnknownToken("two \n lines")


class TestBlankToken:

    blank_tokens = [
        '',
        '    ',
        '\t',
        '    \t    ',
        '    \\\n    ',
    ]

    @pytest.mark.parametrize('raw_value', blank_tokens)
    def test_initialization(self, raw_value):
        assert BlankToken(raw_value) is not None

    @pytest.mark.parametrize('raw_value', blank_tokens)
    def test_specialization(self, raw_value):
        assert type(UnknownToken(raw_value).specialized()) == BlankToken

    def test_invalid(self):
        with pytest.raises(ValueError, match='is not a BlankToken'):
            assert BlankToken('x')


class TestCommentToken:

    comment_tokens = [
        '; simple semicolon style comment',
        '# simple hash style comment',
        '    # an indented comment',
        '    # an indented comment \\\n    containing a wrapped line',
        '#[global]',
        '# [global]',
        '#ignore = me',
        '# ignore = me',
        '# ignore=me',
        '#ignore=me',
    ]

    @pytest.mark.parametrize('raw_value', comment_tokens)
    def test_initialization(self, raw_value):
        assert CommentToken(raw_value) is not None

    @pytest.mark.parametrize('raw_value', comment_tokens)
    def test_specialization(self, raw_value):
        assert type(UnknownToken(raw_value).specialized()) == CommentToken

    def test_invalid(self):
        with pytest.raises(ValueError, match='is not a CommentToken'):
            assert CommentToken('x')


class TestSectionNameToken:

    section_name_tokens = [
        '[simple]',
        '    [ with whitespace ]    ',
        '[extra long \\\n multiline section name]',
    ]

    @pytest.mark.parametrize('raw_value', section_name_tokens)
    def test_initialization(self, raw_value):
        assert SectionNameToken(raw_value) is not None

    @pytest.mark.parametrize('raw_value', section_name_tokens)
    def test_specialization(self, raw_value):
        assert type(UnknownToken(raw_value).specialized()) == SectionNameToken

    invalid_section_name_tokens = [
        'x',
        '[]',
        '[=]',
        '[ = ]',
        '[weird = section = name]',
    ]

    @pytest.mark.parametrize('raw_value', invalid_section_name_tokens)
    def test_invalid(self, raw_value):
        with pytest.raises(ValueError, match='is not a SectionNameToken'):
            assert SectionNameToken(raw_value)


class TestParameterToken:

    parameter_tokens = [
        'key=value',
        '    key = value',
        """    key =\\
                    /long/value/on/separate/line""",
    ]

    @pytest.mark.parametrize('raw_value', parameter_tokens)
    def test_initialization(self, raw_value):
        assert ParameterToken(raw_value) is not None

    @pytest.mark.parametrize('raw_value', parameter_tokens)
    def test_specialization(self, raw_value):
        assert type(UnknownToken(raw_value).specialized()) == ParameterToken

    def test_invalid(self):
        with pytest.raises(ValueError, match='is not a ParameterToken'):
            assert ParameterToken('x')


class TestSambaConfigParser:

    def test_construction(self):
        SambaConfigParser(
            fake_root=test_data / 'simple',
            smb_conf=PurePosixPath('/smb.conf')
        )

    def test_localisation(self):
        conf = SambaConfigParser(
            fake_root=test_data / 'simple',
            smb_conf=PurePosixPath('/smb.conf')
        )

        # The following should give us the basic configuration file,
        # which should exist as part of the test data.
        assert conf.localize(PurePosixPath('/smb.conf')).is_file()

        # However the same string as a WindowsPath is not considered to be
        # absolute so should raise an error.
        with pytest.raises(
                ValueError, match='Relative path passed to `localise`'):
            assert conf.localize(PureWindowsPath('/smb.conf')).is_file()

        # If we wanted to use Windows paths it would still need to be absolute.
        assert conf.localize(PureWindowsPath('C:\\smb.conf')).is_file()

    @pytest.mark.skip
    def test_context(self):
        naive = SambaConfigParser(
            fake_root=test_data / 'simple',
            smb_conf=PurePosixPath('/smb.conf')
        )

        assert naive['global']['log level'] == '1'
        assert naive['global']['log file'] == '/var/log/samba/%m.log'

        contextual = naive.contextualized(MockServer(netloc='NAS.example.com'))

        assert contextual['global']['log level'] == '1'
        assert contextual['global']['log file'] == '/var/log/samba/NAS.log'
