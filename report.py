
import os
import glob
import collections
from pathlib import Path

import coverage
from coverage.files import PathAliases


class CoverageRun:

    base_dir = Path('coverage')
    data_dir = base_dir / 'data'
    report_dir = base_dir / 'reports'
    base_filename = '.' + str(base_dir)

    def __init__(self, filename):
        filename = filename[len(self.base_filename):]
        bits = list(filter(None, filename.split('.')))
        self.facets = bits[0].split('-')
        self.host_name = bits[1]
        self.process_id = bits[2]
        self.random_number = bits[3]

    def __repr__(self):
        return '.'.join(
            [self.base_filename] +
            ['-'.join(self.facets)] +
            [self.host_name, self.process_id, self.random_number]
        )

    @property
    def path(self):
        return self.data_dir / self.__repr__()

    @property
    def data(self):
        cd = coverage.CoverageData()
        cd.read_file(self.path)
        return cd


def main():

    aliases = PathAliases()

    # Source
    # ------
    # Tox, Windows, Conda
    aliases.add(r'.tox\\test-*\\Lib\\site-packages', './src')
    # Tox, Linux, virtualenv
    aliases.add(r'.tox/test-*/lib/*/site-packages', './src')
    # Tox, Linux, Gitlab runner
    aliases.add(r'**/.tox/test-*/lib/*/site-packages', './src')

    # Tests
    # ------
    # Windows
    aliases.add(r'**\\tests', './tests')
    # Linux
    aliases.add(r'**/tests', './tests')

    # Create a dictionary which maps each facet to a list of all the runs which
    # contribute to measuring that facet.
    run_by_facet = collections.defaultdict(list)
    # for filename in os.listdir(CoverageRun.data_dir):
    d = CoverageRun.data_dir / (str(CoverageRun.base_filename) + '*')
    for full_glob in glob.glob(str(d)):
        cr = CoverageRun(os.path.basename(full_glob))
        for facet in cr.facets:
            run_by_facet[facet].append(cr)

    # Combine data for each facet
    combined_facet = 'test'
    combined_total = None
    for facet in run_by_facet:
        facet_data = coverage.CoverageData()
        for run in run_by_facet[facet]:
            facet_data.update(run.data, aliases=aliases)

        facet_dir = CoverageRun.report_dir / facet
        os.makedirs(str(facet_dir), exist_ok=True)
        facet_filename = CoverageRun.base_filename + '.' + facet
        facet_filepath = facet_dir / facet_filename
        facet_data.write_file(filename=str(facet_filepath))

        c = coverage.Coverage(data_file=str(facet_filepath))
        c.load()
        print('\n\n' + facet + '\n' + str('=' * len(facet)) + '\n')
        c.report()
        facet_total = c.html_report(directory=str(facet_dir))

        if facet == combined_facet:
            combined_total = facet_total

    print("\n\nCOMBINED TOTAL: {:.2f}".format(combined_total))


if __name__ == '__main__':
    main()
